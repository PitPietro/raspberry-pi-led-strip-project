import time
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# The number of NeoPixels
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

# increase this value to slow down the effect
delay_time = 0.0005

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))
    
def rgb_loop():
    for j in range(3):
        # fade IN
        for k in range(256):
            if j == 0:
                set_all(k, 0, 0)
            elif j == 1:
                set_all(0, k, 0)
            elif j == 2:
                set_all(0, 0, k)
            show_strip()
            time.sleep(delay_time)
            
        # fade OUT
        for k in reversed(range(256)):
            if j == 0:
                set_all(k, 0, 0)
            elif j == 1:
                set_all(0, k, 0)
            elif j == 2:
                set_all(0, 0, k)
            show_strip()
            time.sleep(delay_time)
            

while True:
    rgb_loop()


# sudo python3 bouncing-RGB-fade.py
