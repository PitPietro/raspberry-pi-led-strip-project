import time
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# The number of NeoPixels
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))
    
def color_wipe(red, green, blue, delay_time):
    for i in range(num_pixels):
        set_pixel(i, red, green, blue)
        show_strip()
        time.sleep(delay_time)
        
def reverse_color_wipe(red, green, blue, delay_time):
    for i in reversed(range(num_pixels)):
        set_pixel(i, red, green, blue)
        show_strip()
        time.sleep(delay_time)
            

while True:
    # to convert an hex string to decimal, use: int("0xaa", 16)
    # replace "0xaa" with the hex number using the "0x" prefix
    color_wipe(int("0xdd", 16), int("0x2c", 16), int("0x00", 16), 0.005)
    color_wipe(int("0x30", 16), int("0x4f", 16), int("0xff", 16), 0.005)
    color_wipe(int("0x00", 16), int("0xbf", 16), int("0xa5", 16), 0.005)
    reverse_color_wipe(int("0xdd", 16), int("0x2c", 16), int("0x00", 16), 0.005)
    reverse_color_wipe(int("0x30", 16), int("0x4f", 16), int("0xff", 16), 0.005)
    reverse_color_wipe(int("0x00", 16), int("0xbf", 16), int("0xa5", 16), 0.005)


# sudo python3 color-wipe.py

