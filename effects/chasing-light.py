import time
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# The number of NeoPixels
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

# increase this value to slow down the effect
delay_time = 0.0005

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))
    
def forward(red, green, blue):
    for i in range(num_pixels):
        set_all(0, 0, 0)
        set_pixel(i-2, green, blue, red)
        set_pixel(i-1, blue, red, green)
        set_pixel(i, red, green, blue)
        show_strip()
        time.sleep(delay_time)
        
def downward(red, green, blue):
    for i in reversed(range(num_pixels)):
        set_all(0, 0, 0)
        set_pixel(i-2, green, blue, red)
        set_pixel(i-1, blue, red, green)
        set_pixel(i, red, green, blue)
        show_strip()
        time.sleep(delay_time)
            

while True:
    # to convert an hex string to decimal, use: int("0xaa", 16)
    # replace "0xaa" with the hex number using the "0x" prefix
    forward(int("0x30", 16), int("0x20", 16), int("0xff", 16));
    downward(int("0x30", 16), int("0x20", 16), int("0xff", 16));


# sudo python3 chasing-light.py

