import time
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# The number of NeoPixels
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))
    
def my_effect():
    pass

while True:
    my_effect()


# sudo python3 test.py
