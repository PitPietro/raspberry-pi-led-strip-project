import time
import math
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# the number of LEDs
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))

def bouncing_balls(red, green, blue, ball_counts):
    gravity = -9.81
    startHeight = 1

    height = [0] * ball_counts
    impactVelocityStart = math.sqrt(-2 * gravity * startHeight)
    impactVelocity = [0] * ball_counts
    timeSinceLastBounce = [0] * ball_counts
    position = [0] * ball_counts
    clockTimeSinceLastBounce = [0] * ball_counts
    dampening = [0] * ball_counts

    for i in range(ball_counts):
        clockTimeSinceLastBounce[i] = round(time.time() * 1000)
        height[i] = startHeight
        position[i] = 0
        impactVelocity[i] = impactVelocityStart
        timeSinceLastBounce[i] = 0
        dampening[i] = 0.90 - float(i) / pow(ball_counts, 2)

    while True:
        for i in range(ball_counts):
            timeSinceLastBounce[i] =  round(time.time() * 1000) - clockTimeSinceLastBounce[i]
            height[i] = 0.5 * gravity * pow(timeSinceLastBounce[i] / 1000, 2.0) + impactVelocity[i] * timeSinceLastBounce[i] / 1000

            if (height[i] < 0):
                height[i] = 0
                impactVelocity[i] = dampening[i] * impactVelocity[i]
                clockTimeSinceLastBounce[i] = round(time.time() * 1000)

                if (impactVelocity[i] < 0.01):
                    impactVelocity[i] = impactVelocityStart
      
            position[i] = round(height[i] * (num_pixels - 1) / startHeight)

        for i in range(ball_counts):
            set_pixel(position[i], red, green, blue);

        show_strip()
        set_all(0, 0, 0)

while True:
    bouncing_balls(255, 0, 0, 3)


# sudo python3 bouncing-balls.py
