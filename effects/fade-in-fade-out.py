import time
import board
import neopixel


# pin connected to the LED strip
pixel_pin = board.D10

# The number of NeoPixels
num_pixels = 100

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

def show_strip():
    pixels.show()

def set_pixel(pixel_number, red, green, blue):
    pixels[pixel_number] = (red, green, blue)

def set_all(red, green, blue):
    pixels.fill((red, green, blue))
        
def fade_in_out(red, green, blue):
    for k in range(num_pixels):
        r = (k / 256.0) * red
        g = (k / 256.0) * green
        b = (k / 256.0) * blue
        
        set_all(int(r), int(g), int(b))
        show_strip()
        time.sleep(0.0001)
        
    for k in reversed(range(num_pixels)):
        r = (k / 256.0) * red
        g = (k / 256.0) * green
        b = (k / 256.0) * blue
        
        set_all(int(r), int(g), int(b))
        show_strip()
        time.sleep(0.0001)

while True:
    fade_in_out(int("0xff", 16), int("0x77", 16), int("0x00", 16))
    fade_in_out(int("0x77", 16), int("0x00", 16), int("0xff", 16))
    fade_in_out(int("0x00", 16), int("0xff", 16), int("0x77", 16))


# sudo python3 color-wipe.py
